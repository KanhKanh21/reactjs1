import React from 'react'

export default SideBar => {
    const ItemSidebar = (props => {
        const {title, iconName} = props
        return (
            <li className='treeview'>
                <a href='#'>
                    <i className={iconName}></i>
                    <span>{title}</span>
                </a>
            </li>
        )
    })
    
    return (
        <aside className='sidebar'>
            <div className='user-panel d-flex p-2'>
                <div className='image '>
                    <img src='https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg' className='rounded-circle mx-2'/>
                </div>
                <div className='info '>
                    <p className='text-white mx-1'>Alexander Pierce</p>
                    <a href='#' role='button' className='text-decoration-none text-white'>
                        <i className="fas fa-circle text-success mx-1"></i>
                        Online
                    </a>
                </div>
            </div>
            <form className='sidebar-form'>
                <div className="input-group mb-3 form-input">
                    <input type="text" className="input-search" placeholder="Search..." />
                    <span className="input-group-btn btn-search mt-1" >
                        <a href='#' role='button'><i class="fas fa-search"></i></a>
                    </span>
                </div>
            </form>
            <p >MAIN NAVIGATION</p>
            <ul className='sidebar-menu'>
                <ItemSidebar iconName='nav-icon fas fa-tachometer-alt' title='Dashboard'/>
                <ItemSidebar iconName='nav-icon fas fa-copy' title='Layout Options'/>
                <ItemSidebar iconName='nav-icon fas fa-th' title='Widgets'/>
                <ItemSidebar iconName='nav-icon fas fa-chart-pie' title='Charts'/>
                <ItemSidebar iconName='fas fa-desktop' title='UI Elements'/>
                <ItemSidebar iconName='nav-icon fas fa-edit' title='Forms'/>
                <ItemSidebar iconName='nav-icon fas fa-table' title='Tables'/>
                <ItemSidebar iconName='nav-icon far fa-calendar-alt' title='Calender'/>
                <ItemSidebar iconName='fas fa-envelope' title='Mailbox'/>
                <ItemSidebar iconName='fas fa-folder' title='Examples'/>
                <ItemSidebar iconName='fas fa-share' title='Multilevel'/>
                <ItemSidebar iconName='fas fa-book' title='Documentation'/>
            </ul>
            <p >LABLE</p>
            <ul className='sidebar-menu'>
                <ItemSidebar iconName='far fa-circle text-danger' title='Important'/>
                <ItemSidebar iconName='far fa-circle text-warning' title='Warning'/>
                <ItemSidebar iconName='far fa-circle text-info' title='Infomation'/>
            </ul>
        </aside>
    )
}
