
import React from 'react'

export default Header => {
    return (
        <header className='header d-md-flex '>
            <div className='logo d-flex align-items-center justify-content-center'>
                <h4 className='text-white'>AdminLTE</h4> 
            </div>
            <div className='navbar d-flex align-items-center'>
                <a  href='#' role='button' className='bars-icon text-white pb-3' data-toggle='push-menu'>
                    <i class="fas fa-bars"></i>
                </a>
                <div className='navbar-menu '>
                    <ul>
                        <a href='#' role='button'><i className="far fa-envelope text-white"></i></a>
                        <a href='#' role='button'><i className="far fa-bell text-white"></i></a>
                        <a href='#' role='button'><i className="far fa-flag text-white"></i></a>
                        <a className='user text-decoration-none' href='#' role='button'>
                            <img src='https://adminlte.io/themes/AdminLTE/dist/img/user2-160x160.jpg' className='rounded-circle'/>
                            <span className='user-name text-white'>Alexander Pierce</span>
                        </a>
                        <a href='#' role='button'><i class="fas fa-cogs text-white"></i></a>
                    </ul>
                </div>
            </div>
        </header>
    ) 
}
