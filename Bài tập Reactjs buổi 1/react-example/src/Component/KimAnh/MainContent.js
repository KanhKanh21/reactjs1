import React from 'react'
import * as ReactBootStrap from 'react-bootstrap'

export default function MainContent() {
    const item = [
        {engine: 'Gecko', browser: 'Firefox 1.0', platform:'Win 98+ / OSX.2+', version: '1.7', css: 'A'},
        {engine: 'Gecko', browser: 'Firefox 1.0', platform:'Win 98+ / OSX.2+', version: '1.8', css: 'A'},
        {engine: 'Gecko', browser: 'Firefox 2.0', platform:'Win 98+ / OSX.2+', version: '1.8', css: 'A'},
        {engine: 'Gecko', browser: 'Firefox 3.0', platform:'Win 2k+ / OSX.3+', version: '1.9', css: 'A'},
        {engine: 'Gecko', browser: 'Camino 1.0', platform:'OSX.2+', version: '1.8', css: 'A'},
        {engine: 'Gecko', browser: 'Camino 1.5', platform:'OSX.3+', version: '1.8', css: 'A'},
        {engine: 'Gecko', browser: 'Nestcape 7.2', platform:'Win 95+ / Mac OS 8.6-9.2', version: '1.7', css: 'A'},
        {engine: 'Gecko', browser: 'Netscape Navigator 8', platform:'Win 98SE+', version: '1.7', css: 'A'},
        {engine: 'Gecko', browser: 'Netscape Navigator 9', platform:'Win 98+ / OSX.2+', version: '1.8', css: 'A'},
        {engine: 'Gecko', browser: 'Mozilla 1.0', platform:'Win 95+ / OSX.1+', version: '1', css: 'A'}
    ]
    const renderItem = (item, index) => {
        return (
            <tr key = {index}>
                <td>{item.engine}</td>
                <td>{item.browser}</td>
                <td>{item.platform}</td>
                <td>{item.version}</td>
                <td>{item.css}</td>
            </tr>
        )
    }
    return (
        <div className='content-main bg-white p-2 border-top border-3 rounded-1'>
            <h4 className='fs-6'>Hover Data Tables</h4>
            <div className='table'>
            <ReactBootStrap.Table bordered hover>
                <thead>
                    <tr>
                        <th>Rendering engine</th>
                        <th>Browser</th>
                        <th>Platform(s)</th>
                        <th>Engine version</th>
                        <th>CSS grade</th>
                    </tr>
                </thead>
                <tbody>
                    {item.map(renderItem)}
                </tbody>
            </ReactBootStrap.Table>
            </div>
        </div>
    )
}
