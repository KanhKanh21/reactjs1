import React from 'react'

export default function Footer() {
    return (
        <footer className='main-footer'>
            <div className='pull-right hidden-xs'>
                <b className='mx-1'>Version</b>
                2.4.13
            </div>
            <strong className='px-1'>
                Copyright © 2014-2019
                <a href='https://adminlte.io' className='text-decoration-none link-admin'>AdminLTE</a>
                .
            </strong>
            All rights reserved.
        </footer>
    )
}
