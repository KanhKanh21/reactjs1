import React from 'react'

export default function HeaderContent() {
    const ItemHeaderContent = (props => {
        const {title, iconName} = props
        return (
            <li className='item-header-content'>
                <a href='#' className='text-decoration-none text-dark '>
                    <i className={iconName}></i>
                    {title}
                </a>
            </li>
        )
    })
    return (
        <div className='content-header my-3 d-flex '>
           <div className='title-header'>
               Data Tables
               <small className='advance-tables'>advanced tables</small>
           </div>
           <div className='breadcrumb d-flex align-items-center rounded-1'>
                <ItemHeaderContent iconName='fas fa-tachometer-alt mx-2' title='Home &nbsp;&nbsp;> &nbsp;&nbsp; '/>
                <ItemHeaderContent title='Tables &nbsp;&nbsp;> &nbsp;&nbsp;'/>
                <ItemHeaderContent title='Data Tables'/>
           </div>
        </div>
    )
}
