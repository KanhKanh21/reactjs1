import logo from './logo.svg';
import './css/header.css';
import './css/sidebar.css';
import './css/content.css';
import './css/footer.css'
import Header from './Component/KimAnh/Header';
import SideBar from './Component/KimAnh/SideBar';
import HeaderContent from './Component/KimAnh/HeaderContent';
import Footer from './Component/KimAnh/Footer';
import MainContent from './Component/KimAnh/MainContent';

function App() {
  return (
    <div>
      <Header />
      <div className='content d-flex'>
        <SideBar />
        <div className='mx-3 content-table'>
          <HeaderContent />
          <MainContent />
        </div>
      </div>
      <Footer />
    </div>
  );
}

export default App;
